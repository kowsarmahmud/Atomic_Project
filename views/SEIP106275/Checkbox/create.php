   
<?php

function __autoload($className) {
    $fileName = "../../../" . str_replace("\\", "/", $className) . '.php';
    //echo $fileName;
    include_once ($fileName);
}

use src\Bitm\SEIP106275\Checkbox\Hobby;

$hobby = new Hobby();
echo "<br>";
//$hobby->index();
?>

<?php include_once 'layout/header.php'; ?>


<div class="container">
    <div class="row">
        <div class="well">
            <div style="height: 400px" class="container-fluid">
                <div class="row centered">
                    <div class="col-sm-4 col-md-4">
                    </div>
                    <div style="padding-top: 40px" class="col-sm-4 col-md-4">
                        <div class="centered center-block">
                            <form class="form-inline " role="form">
                                <div class="form-group">
                                    <label for="text">Hobby Name:</label>
                                    <input type="text" class="form-control" placeholder="input hobby name" id="text">
                                </div>           
                                <button type="submit" class="btn btn-default">Create</button>
                            </form>
                            <h1><?php $hobby->create(); ?></h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-4 col-md-4">
    </div>
</div>


<?php include_once 'layout/footer.php'; ?>

