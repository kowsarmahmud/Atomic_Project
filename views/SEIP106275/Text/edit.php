
<?php

function __autoload($className) {
    $fileName = "../../../" . str_replace("\\", "/", $className) . '.php';
    //echo $fileName;
    include_once ($fileName);
}

use src\Bitm\SEIP106275\Text\Book;

$book = new Book();
echo "<br>";
//$book->index();
?>

<?php include_once 'layout/header.php'; ?>
      
        <div class="container">
            <div class="row">
                <div class="well">
                    <div style="height: 400px">
                        <div class="col-sm-4 col-md-4">
                        </div>
                        <div style="padding-top: 40px" class="col-sm-4 col-md-4">    
                            <div class="centered center-block">
                                <form class="form-inline" role="form">
                                    <div class="form-group">
                                        <label for="text">Book Name:</label>
                                        <input type="text" class="form-control" value="Islamic History" id="text">
                                    </div>           
                                    <button type="submit" class="btn btn-default">Update</button>
                                    <hr>
                                    <div><a href="list.php">List</a>&NonBreakingSpace; &NonBreakingSpace;<a href="delete.php">Delete</a></div>
                                </form>
                                <h2><?php $book->edit(); ?></h2>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-4">
                    </div>
                </div>
            </div>
        </div>
          
        <?php include_once 'layout/footer.php'; ?>
