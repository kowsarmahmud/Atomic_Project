
<?php

function __autoload($className) {
    $fileName = "../../../" . str_replace("\\", "/", $className) . '.php';
    //echo $fileName;
    include_once ($fileName);
}

use src\Bitm\SEIP106275\File\Picture;

$picture = new Picture();
echo "<br>";
//$picture->delete();
?>

<?php include_once 'layout/header.php'; ?>

<div class="container">
    <div class="row">
        <div class="well">
            <div style="height: 400px">
                <div class="col-sm-4 col-md-4">
                </div>
                <div style="padding-top: 40px" class="col-sm-4 col-md-4">    
                    <div class="centered center-block">
                        <form action="#" method="post" enctype="multipart/form-data">
                            <label for="book">Profile Picture:</label>
                            <input type="file" name="file">
                            <input type="submit" name="name" value="Upload" >
                            <hr>
                            <div><a href="list.php">List</a>&NonBreakingSpace; &NonBreakingSpace;<a href="delete.php">Delete</a></div>
                        </form>  
                        <h2><?php $picture->edit(); ?></h2>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 col-md-4">
            </div>
        </div>
    </div>
</div>

<?php include_once 'layout/footer.php'; ?>
