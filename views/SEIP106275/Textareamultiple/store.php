   
<?php

function __autoload($className) {
    $fileName = "../../../" . str_replace("\\", "/", $className) . '.php';
    //echo $fileName;
    include_once ($fileName);
}

use src\Bitm\SEIP106275\Textareamultiple\Bookmark;

$bookmark = new Bookmark();
echo "<br>";
//$hobby->index();
?>   
<?php include_once 'layout/header.php'; ?>

<div class="container">
    <div class="row">
        <div class="well">
            <div style="height: 400px" class="col-ms-3">
                <h2><?php $bookmark->edit(); ?></h2>
                <table class="table table-striped" border="1" align="center" style="width:60%">
                    <caption style="text-align: center; color: black; font-size: 14;">Name of Book marking</caption>
                    <tr>
                        <th>ID</th>
                        <th>Book Marking</th>
                        <th>Action</th>
                    </tr>
                    <tr>
                        <td>01</td>
                        <td>www.google.com</td>
                        <td><a href="view.php">View</a>&nbsp;&nbsp;<a href="edit.php">Edit</a>&nbsp;&nbsp;<a href="delete.php">Delete</a></td>
                    </tr>
                    <tr>
                        <td>02</td>
                        <td>www.yahoo.com</td>
                        <td><a href="view.php">View</a>&nbsp;&nbsp;<a href="edit.php">Edit</a>&nbsp;&nbsp;<a href="delete.php">Delete</a></td>
                    </tr>
                    <tr>
                        <td>03</td>
                        <td>www.bing.com</td>
                        <td><a href="view.php">View</a>&nbsp;&nbsp;<a href="edit.php">Edit</a>&nbsp;&nbsp;<a href="delete.php">Delete</a></td>
                    </tr>
                </table>
                <div style="margin-left: 185px"><a href="create.php">Add Bookmarking</a></div>
                <nav style="float: right; padding-right: 190px">
                    <ul class="pagination">
                        <li>
                            <a href="#" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li>
                            <a href="#" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>

<?php include_once 'layout/footer.php'; ?>
