<?php

namespace src\Bitm\SEIP106275\Checkbox;

class Hobby {

    public $id;
    public $title;
    public $created;
    public $modified;
    public $created_by;
    public $modified_by;
    public $deleted_at;

    public function __construct($model = false) {
       
    }

    public function index() {
        echo 'I am listing data';
    }

    public function create() {
        echo 'I am create form';
    }

    public function store() {
        echo 'I am storing data';
    }

    public function edit() {
        echo 'I am editing data';
    }

    public function update() {
        echo 'I am updateing data';
    }

    public function delete() {
        echo 'I am delete data';
    }

}
